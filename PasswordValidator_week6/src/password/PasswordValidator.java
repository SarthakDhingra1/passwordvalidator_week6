package password;

/**
 * 
 * @author Sarthak Dhingra - 991521229 Validates passwords and is being
 *         developed using TDD
 */
public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS =2;

	public static boolean hasValidCaseChars(String password) {
		return password!=null && password.matches( "^.*[a-z].*$" ) && password.matches( "^.*[A-Z].*$" ); // checks for uppercase and lowercase

	}
	
	
	
	public static boolean isVaildLength(String password) {
		/*
		 * if(password.indexOf(" ")>=0) { // because index starts at 0 , return false; }
		 */
		return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH;
		// one thing about && is , it only checks the first condition first
		// if it fails it doesnt check the next one

	}

	public static boolean IsValidDigits(String password) {

		int count = 0;
		for (char c : password.toCharArray()) {
			if (Character.isDigit(c)) {
				count++;
				if (count == 2) {
					return true;
				}
			}
		}
		return false;
	}


}