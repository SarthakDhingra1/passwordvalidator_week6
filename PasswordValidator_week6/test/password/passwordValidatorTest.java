package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Sarthak Dhingra - 991521229
 * Validates passwords and is being developed using TDD
 */

public class passwordValidatorTest {

	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars",PasswordValidator.hasValidCaseChars("aBAaksiB"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars",PasswordValidator.hasValidCaseChars("hY"));
	}
	
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars",PasswordValidator.hasValidCaseChars("BAJDJNASK"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars",PasswordValidator.hasValidCaseChars("ndfnknkdn"));
	}
	
	
	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		assertFalse("Invalid case chars",PasswordValidator.hasValidCaseChars("9141419"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionSpecialCharacters() {
		assertFalse("Invalid case chars",PasswordValidator.hasValidCaseChars("@#$%^&*"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		assertFalse("Invalid case chars",PasswordValidator.hasValidCaseChars(" "));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Invalid case chars",PasswordValidator.hasValidCaseChars(null));
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////
	@Test
	public void testIsVaildLengthRegular() {
		boolean result=PasswordValidator.isVaildLength("1234567890");
		assertTrue("The length is invalid",result);
	}
	
	@Test
	public void testIsVaildLengthException() {
		boolean result=PasswordValidator.isVaildLength("");
		assertFalse("Invalid length",result);
	}
	
	@Test
	public void testIsVaildLengthExceptionSpaces() {
		boolean result=PasswordValidator.isVaildLength("    test     ");
		assertFalse("Invalid length",result);
	}
	
	@Test
	public void testIsVaildLengthExceptionSpacesInBetweenString() {
		boolean result=PasswordValidator.isVaildLength("    t  e  s  t     ");
		assertFalse("Invalid length",result);
	}
	
	
	
	@Test
	public void testIsVaildLengthBoundaryIn() {
		boolean result=PasswordValidator.isVaildLength("12345678");
		assertTrue("The length is invalid",result);
		
	}
	
	
	@Test
	public void testIsVaildLengthBoundaryOut() {
		boolean result=PasswordValidator.isVaildLength("1234567");
		assertFalse("The length is invalid",result);
			
	}
	
	
	@Test
	public void testIsValidDigitsRegular() {
		boolean result=PasswordValidator.IsValidDigits("Sarthak1234");
				assertTrue("Invalid number of Digits",result);
	}
	
	
	@Test
	public void testIsValidDigitsException() {
		boolean result=PasswordValidator.IsValidDigits("Sarthak$$");
				assertFalse("Invalid number of Digits",result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryIn() {
		boolean result=PasswordValidator.IsValidDigits("Sarthak12");
		assertTrue("Invalid number of Digits",result);
		
	}

	@Test
	public void testIsValidDigitsBoundaryOut() {
		boolean result=PasswordValidator.IsValidDigits("Sar1hak");
		assertFalse("Invalid number of Digits",result);
		
	}
	
}
